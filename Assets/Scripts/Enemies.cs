﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Enemies : MonoBehaviour {
    public static List<GameObject> enemies = new List<GameObject>();
    
    [FormerlySerializedAs("Enemy")] public GameObject enemy;

    [SerializeField]
    private float spawnTime;
        
    private float nextTimeToSpawn;

    private void Start() {
        nextTimeToSpawn = Time.time;
    }

    private void Spawn() {
        var newEnemy = Instantiate(enemy);
        newEnemy.transform.position = MapGenerator.startTile.transform.position;
        enemies.Add(enemy);
    }

    private void Update() {
        if (Time.time >= nextTimeToSpawn) {
            Spawn();
            nextTimeToSpawn = Time.time + spawnTime;
        }
    }
}