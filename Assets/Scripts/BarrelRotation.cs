using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelRotation : MonoBehaviour {
    public Transform pivot;

    public Transform barrel;

    public Tower tower;

    // Start is called before the first frame update
    void Start() { }

    // Update is called once per frame
    void Update() {
        if (tower != null) {
            if (tower.currentTarget != null) {
                // From tower to enemy
                Vector3 relative = tower.currentTarget.transform.position - pivot.position;
                Quaternion lookRotation = Quaternion.LookRotation(relative);
                Vector3 rotation = Quaternion.Slerp(pivot.rotation, lookRotation, Time.deltaTime*10).eulerAngles;

                pivot.rotation = Quaternion.Euler(0, rotation.y, 0);
            }
        }
    }
}