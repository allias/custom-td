using UnityEngine;

public class Tower : MonoBehaviour {
    [SerializeField] private float range;
    [SerializeField] private float damage;
    [SerializeField] private float reloadTime;

    public GameObject currentTarget;
    private float nextTimeToShoot;

    private void Start() {
        nextTimeToShoot = Time.time;
    }

    private void Update() {
        updateNearestEnemy();
        if (Time.time >= nextTimeToShoot)
            if (currentTarget != null) {
                shoot();
                nextTimeToShoot = Time.time + reloadTime;
            }
    }

    private void updateNearestEnemy() {
        GameObject currentNearestEnemy = null;
        var distanceToNearestEnemy = Mathf.Infinity;

        foreach (var enemy in Enemies.enemies) {
            var distanceToEnemy = (transform.position - enemy.transform.position).magnitude;

            if (distanceToEnemy < distanceToNearestEnemy) {
                distanceToNearestEnemy = distanceToEnemy;
                currentNearestEnemy = enemy;
            }
        }

        Debug.Log(distanceToNearestEnemy + " ?=? " + range);
        if (distanceToNearestEnemy <= range)
            currentTarget = currentNearestEnemy;
        else
            currentTarget = null;
    }

    private void shoot() {
        if (currentTarget != null) {
            var enemyScript = currentTarget.GetComponent<Enemy>();
            enemyScript.TakeDamage(damage);
        }
    }
}