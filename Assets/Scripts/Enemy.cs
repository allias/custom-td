using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    [SerializeField] private float enemyHealth;
    [SerializeField] private float movementSpeed;
    private int damage;
    private int killReward;

    private LinkedListNode<GameObject> targetTile;

    private void Awake() {
        Enemies.enemies.Add(gameObject);
    }

    private void Start() {
        InitializeEnemy();
    }

    private void Update() {
        CheckPosition();
        Move();
        TakeDamage(0);
    }

    private void InitializeEnemy() {
        targetTile = MapGenerator.pathTiles.First;
    }

    public void TakeDamage(float amount) {
        enemyHealth -= amount;

        if (enemyHealth <= 0) Die();
    }

    private void Die() {
        Enemies.enemies.Remove(gameObject);
        Destroy(transform.gameObject);
    }

    private void Move() {
        transform.position = Vector3.MoveTowards(
            transform.position,
            targetTile.Value.transform.position,
            movementSpeed * Time.deltaTime
        );
    }

    private void CheckPosition() {
        if (targetTile != null) {
            var distance = (transform.position - targetTile.Value.transform.position).magnitude;

            if (distance < 0.001f) {
                if (targetTile.Next != null)
                    targetTile = targetTile.Next;
                else Die(); // die on last tile of the path
            }
        }
    }
}