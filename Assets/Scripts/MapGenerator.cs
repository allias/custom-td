using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour {
    public static GameObject[,] mapTiles;
    public static LinkedList<GameObject> pathTiles = new LinkedList<GameObject>();
    public static GameObject startTile;
    public static GameObject endTile;
    [SerializeField] private int mapWidth;
    [SerializeField] private int mapHeight;

    [FormerlySerializedAs("MapTile")] public GameObject mapTile;
    public Color pathColor;
    public Color startColor;
    public Color endColor;

    private void Start() {
        GenerateMap();
    }

    private void GenerateMap() {
        mapTiles = new GameObject[mapWidth, mapHeight];
        var mapTileScale = mapTile.gameObject.transform.lossyScale;
        for (var z = 0; z < mapHeight; z++)
        for (var x = 0; x < mapWidth; x++) {
            var newTile = Instantiate(mapTile);
            newTile.transform.position = new Vector3(x * (mapTileScale.x + 1) - mapTileScale.x / 2, 0,
                z * (mapTileScale.z + 1) + mapTileScale.z / 2);
            mapTiles[x, z] = newTile;
        }

        var directions = Enum.GetValues(typeof(Direction)).Cast<Direction>().ToList();

        var currentX = 0;
        var currentY = 0;
        while (true) {
            pathTiles.AddLast(mapTiles[currentX, currentY]);
            if (currentX == mapWidth - 1 && currentY == mapHeight - 1) break;

            var randomDirection = directions[Random.Range(0, directions.Count)];

            switch (randomDirection) {
                case Direction.Up:
                    if (currentY + 1 <= mapHeight - 1) currentY += 1;
                    break;
                case Direction.Right:
                    if (currentX + 1 <= mapWidth - 1) currentX += 1;
                    break;
            }
        }

        foreach (var pathTile in pathTiles) {
            var pathTileRenderer = pathTile.GetComponent<Renderer>();
            pathTileRenderer.material.color = pathColor;
        }

        startTile = pathTiles.First();
        endTile = pathTiles.Last();
        pathTiles.First().GetComponent<Renderer>().material.color = startColor;
        pathTiles.Last().GetComponent<Renderer>().material.color = endColor;
        Debug.Log("Map generation complete");
    }

    private enum Direction {
        Up,
        Right
    }
}